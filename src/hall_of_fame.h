#ifndef hall_of_fame_h
#define hall_of_fame_h


/* DECLARATION DES TYPES PERSONNELS */


//choix d'une tête fictive 

typedef struct cellule {
   int score;
   char *nom, *alias;
   struct cellule  *suivant;
}cellule_t, *donnee_t;

/* DECLARATIONS DES METHODES */
void afficherDonnee(FILE *, cellule_t);
void saisirDonnee (FILE * , cellule_t *);
cellule_t *lire_cellule(FILE*);
int tableauFromFilename(const char *, cellule_t []);
void afficherliste(cellule_t *p);
void insererliste(signed char nom, signed char alias, int score, cellule_t *p);
void affichageTableau(cellule_t tab[]);
void liberer1Liste(cellule_t *p);
cellule_t * creeListe();
void saisirDonnee2(const char * fic, cellule_t *);
void insererListe(cellule_t * p, donnee_t d);

// mettre ici les autres declarations
#define TAILLE_MAX 50

#endif